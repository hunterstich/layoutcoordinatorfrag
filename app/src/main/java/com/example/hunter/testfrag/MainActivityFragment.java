package com.example.hunter.testfrag;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.annotation.Nullable;
import android.support.v13.app.FragmentPagerAdapter;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private int[] mTabTitles = new int[] {
            R.string.title_1,
            R.string.title_2,
            R.string.title_3
    };

    public CollapsibleHeaderLayout mCollapsibleHeaderLayout;

    private ViewPager mViewPager;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mCollapsibleHeaderLayout = (CollapsibleHeaderLayout) view.findViewById(R.id.collapsible_header_layout);
        mViewPager = (ViewPager) view.findViewById(R.id.view_pager);

        mCollapsibleHeaderLayout.setSlidingTabLayoutContentDescriptions(mTabTitles);

        FragmentViewPagerAdapter fragmentViewPagerAdapter = new FragmentViewPagerAdapter(getChildFragmentManager());
        mViewPager.setAdapter(fragmentViewPagerAdapter);
        mCollapsibleHeaderLayout.setViewPager(mViewPager);
    }

    //make the header layout accessible to the viewpager's fragments
    public CollapsibleHeaderLayout getCollapsibleHeaderLayout() {
        return mCollapsibleHeaderLayout;
    }

    private class FragmentViewPagerAdapter extends FragmentPagerAdapter {

        public FragmentViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public android.app.Fragment getItem(int i) {
            SampleFragment sampleFragment = new SampleFragment();
            Bundle args = new Bundle();
            args.putString(SampleFragment.KEY_FRAGMENT_TITLE, getString(mTabTitles[i]));
            sampleFragment.setArguments(args);
            return sampleFragment;

        }

        @Override
        public int getCount() {
            return mTabTitles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getString(mTabTitles[position]);
        }

    }
}
